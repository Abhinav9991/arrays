const  flatten=(array,depth)=>{
    let result = [];
    if(depth===undefined){
        depth=1
    }
    for(let index = 0; index < array.length; index++) {
        if(Array.isArray(array[index]) && depth>0) {
            result = result.concat(flatten(array[index],depth-1));
        } else if(array[index]!=undefined) {
            result.push(array[index]);
        }
    }
    return result;
}

module.exports=flatten
