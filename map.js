const map=(items,cb)=>{
    if(!items || !cb){
        return []}
    else{
    let output=[]
    for(let index=0;index<items.length;index++){
        output.push(cb(items[index],index,items));
    }
    return output;}
}

module.exports=map