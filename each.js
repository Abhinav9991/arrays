const each=(items,cb)=>{
    if(!items || !cb){
        return []}
    else{
    let output=[]
    for(let i=0;i<items.length;i++){
        output.push(cb(items,i));
    }
    return output;}
}

module.exports=each