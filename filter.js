const filter=(elements,cb)=>{
    if(!elements || !cb){
        return []
    }
    else{
        let output=[]
        for(let index=0; index<elements.length; index++){
            if(cb(elements[index],index,elements)===true){
                output.push(elements[index])
            }
        }
        return output;
    }
}
module.exports=filter